﻿/// <reference path="~/tests/qunit.js" />
/// <reference path="~/game/hunting_grounds.js" />

QUnit.test("Sending zero hunters to sendHunters throws exception", function (assert) {
    var huntingGrounds = new HuntingGrounds();
    
    assert.throws(
        function () {
            huntingGrounds.sendHunters(0, new Tribe("drew"));
        }, 
        function (err) {
            return err === "Cannot send zero hunters";
        }
    );
});

QUnit.test("HuntingGrounds remembers hunters from previous sendHunters", function (assert) {
    var huntingGrounds = new HuntingGrounds();
    var tribe = new Tribe("drew");

    huntingGrounds.sendHunters(3, tribe);

    assert.deepEqual(huntingGrounds.numHunters(tribe), 3);
});

QUnit.test("HuntingGrounds does not allow the same tribe to send hunters twice", function (assert) {
    var huntingGrounds = new HuntingGrounds();
    var tribe = new Tribe("drew");

    huntingGrounds.sendHunters(1, tribe);

    assert.throws(
        function () {
            huntingGrounds.sendHunters(1, tribe);
        },
        function (err) {
            return err === "Same tribe cannot send hunters twice";
        }
    );
});

QUnit.test("HuntingGrounds.numHunters returns 0 hunters for undefined tribe", function (assert) {
    var huntingGrounds = new HuntingGrounds();
    assert.deepEqual(huntingGrounds.numHunters(undefined), 0);
});

QUnit.test("HuntingGrounds.numHunters returns 0 hunters for tribe not yet hunting", function (assert) {
    var huntingGrounds = new HuntingGrounds();
    assert.deepEqual(huntingGrounds.numHunters(new Tribe("drew")), 0);
});

QUnit.test("HuntingGrounds.clear removes hunters", function (assert) {
    var huntingGrounds = new HuntingGrounds();
    var tribe = new Tribe("drew");
    huntingGrounds.sendHunters(2, tribe);
    huntingGrounds.clear();
    assert.deepEqual(huntingGrounds.numHunters(tribe), 0);
});

QUnit.test("HuntingGrounds.hunt rolls die per hunter", function (assert) {
    var huntingGrounds = new HuntingGrounds();
    var tribe = new Tribe("drew");
    huntingGrounds.sendHunters(3, tribe);

    var timesRolled = 0;
    var d6 = new D6();

    huntingGrounds.d6.roll = function () {
        timesRolled += 1;
        return d6.roll();
    };

    huntingGrounds.hunt(tribe);
    assert.deepEqual(timesRolled, 3);
});

QUnit.test("Hunting grounds hunt returns Nd6 / 2 food", function (assert) {
    var huntingGrounds = new HuntingGrounds();
    var tribe = new Tribe("drew");
    huntingGrounds.sendHunters(3, tribe);

    var d6 = new D6();

    huntingGrounds.d6.roll = function () {
        return 2;
    };

    var food = huntingGrounds.hunt(tribe);
    assert.deepEqual(food, 3);
});

QUnit.test("Hunting grounds hunt drops remainder", function (assert) {
    var huntingGrounds = new HuntingGrounds();
    var tribe = new Tribe("drew");
    huntingGrounds.sendHunters(3, tribe);

    var d6 = new D6();

    huntingGrounds.d6.roll = function () {
        return 3;
    };

    var food = huntingGrounds.hunt(tribe);
    assert.deepEqual(food, 4);
});

QUnit.test("Tribe with no hunters hunts and gets zero food", function (assert) {
    var hg = new HuntingGrounds();
    var food = hg.hunt(new Tribe("drew"));
    assert.deepEqual(food, 0);
});

QUnit.test("Sending hunters uses available tribesmen", function (assert) {
    var hg = new HuntingGrounds();
    var tribe = new Tribe("drew");
    var availableTribesmen = tribe.availableTribesmen();
    hg.sendHunters(3, tribe);
    assert.deepEqual(availableTribesmen - 3, tribe.availableTribesmen());
});

QUnit.test("Trying to send too many hunters does not remember hunters", function (assert) {
    var hg = new HuntingGrounds();
    var tribe = new Tribe("drew");

    try {
        hg.sendHunters(tribe.availableTribesmen() + 1, tribe)
    } catch (e) { };

    assert.deepEqual(0, hg.numHunters(tribe));
});

QUnit.test("Trying to send too many hunters does not consume tribesmen", function (assert) {
    var hg = new HuntingGrounds();
    var tribe = new Tribe("drew");

    var availableTribesmen = tribe.availableTribesmen();

    try {
        hg.sendHunters(tribe.availableTribesmen() + 1, tribe)
    } catch(e) { };

    assert.deepEqual(availableTribesmen, tribe.availableTribesmen());
});

QUnit.test("Hunting returns tribesmen to available", function (assert) {
    var hg = new HuntingGrounds();
    var tribe = new Tribe("drew");

    var availableTribesmen = tribe.availableTribesmen();

    hg.sendHunters(3, tribe);
    hg.hunt(tribe);

    assert.deepEqual(availableTribesmen, tribe.availableTribesmen());
});

QUnit.test("Hunting removes hunters from grounds", function (assert) {
    var hg = new HuntingGrounds();
    var tribe = new Tribe("drew");

    hg.sendHunters(3, tribe);
    hg.hunt(tribe);

    assert.deepEqual(0, hg.numHunters(tribe));
});

QUnit.test("Hunting adds food to tribe", function (assert) {
    var hg = new HuntingGrounds();
    var tribe = new Tribe("drew");

    var foodStored = tribe.foodStored();

    hg.d6.roll = function () {
        return 2;
    };

    hg.sendHunters(1, tribe);
    hg.hunt(tribe);

    assert.deepEqual(foodStored + 1, tribe.foodStored());
});