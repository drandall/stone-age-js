﻿/// <reference path="~/tests/qunit.js" />
/// <reference path="~/game/turn_order.js" />
/// <reference path="~/game/tribe.js" />

QUnit.test("turn order tribeNames must be array", function (assert) {
    assert.throws(
        function () {
            new TurnOrder("not an array");
        },
        function (err) {
            return err === "tribeNames must be an array";
        }
    );
});

QUnit.test("turn order tribeNames must have at least two tribes", function (assert) {
    assert.throws(
        function () {
            new TurnOrder([]);
        },
        function (err) {
            return err === "tribeNames must have at least two tribes";
        }
    );
});

QUnit.test("turn order tribeNames must have less than five tribes", function (assert) {
    assert.throws(
        function () {
            new TurnOrder(["1", "2", "3", "4", "5"]);
        },
        function (err) {
            return err === "tribeNames must have less than five tribes";
        }
    );
});

QUnit.test("turn order tribeNames must be strings", function (assert) {
    assert.throws(
        function () {
            new TurnOrder([new function () { }, "2"]);
        },
        function (err) {
            return err === "tribeNames must be strings";
        }
    );
});

QUnit.test("tribes get added to remainingTribes", function (assert) {
    var tribes = ["1", "2", "3", "4"];
    var turnOrder = new TurnOrder(tribes.slice());
    assert.notDeepEqual(tribes.indexOf[turnOrder.remainingTribes()[0].name], -1);
    assert.notDeepEqual(tribes.indexOf[turnOrder.remainingTribes()[1].name], -1);
    assert.notDeepEqual(tribes.indexOf[turnOrder.remainingTribes()[2].name], -1);
    assert.notDeepEqual(tribes.indexOf[turnOrder.remainingTribes()[3].name], -1);
});

QUnit.test("turn order starts on sending tribesmen phase", function (assert) {
    var turnOrder = new TurnOrder(["1", "2"]);
    assert.deepEqual(turnOrder.phase(), "sending tribesmen");
});

QUnit.test("end turn goes to next tribe", function (assert) {
    var turnOrder = new TurnOrder(["1", "2"]);
    var currentTribe = turnOrder.currentTribe();
    turnOrder.endTurn();
    assert.notDeepEqual(turnOrder.currentTribe().name, currentTribe.name);
});

QUnit.test("end turn hides tribe when out of tribesmen", function (assert) {
    var turnOrder = new TurnOrder(["1", "2"]);
    var currentTribe = turnOrder.currentTribe();
    currentTribe.useTribesmen(currentTribe.availableTribesmen());
    turnOrder.endTurn();
    var remainingTribes = turnOrder.remainingTribes();
    assert.deepEqual(remainingTribes.indexOf(currentTribe), -1);
});