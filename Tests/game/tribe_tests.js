﻿/// <reference path="~/tests/qunit.js" />
/// <reference path="~/game/tribe.js" />

QUnit.test("Tribe starts with 5 tribesmen", function (assert) {
    var tribe = new Tribe("drew");
    assert.deepEqual(tribe.sizeOfTribe(), 5);
});

QUnit.test("Tribe starts with 5 available tribesmen", function (assert) {
    var tribe = new Tribe("drew");
    assert.deepEqual(tribe.availableTribesmen(), 5);
});

QUnit.test("useTribesmen reduces availableTribesmen for tribe", function (assert) {
    var tribe = new Tribe("drew");
    var availableTribesmen = tribe.availableTribesmen();
    tribe.useTribesmen(1);
    assert.deepEqual(availableTribesmen - 1, tribe.availableTribesmen());
});

QUnit.test("useTribesmen throws error when trying to use more than are available", function (assert) {
    var tribe = new Tribe("drew");

    assert.throws(
        function () {
            tribe.useTribesmen(11);
        },
        function (error) {
            return error === "Not enough available tribesmen";
        }
    );
});

QUnit.test("returnTribesmen increments tribesmen", function (assert) {
    var tribe = new Tribe("drew");
    var availableTribesmen = tribe.availableTribesmen();
    tribe.useTribesmen(3);
    tribe.returnTribesmen(3);
    assert.deepEqual(availableTribesmen, tribe.availableTribesmen());
});

QUnit.test("Tribe starts with 12 food", function (assert) {
    var tribe = new Tribe("drew");
    assert.deepEqual(12, tribe.foodStored());
});

QUnit.test("addFood adds food to tribe", function (assert) {
    var tribe = new Tribe("drew");
    var initFood = tribe.foodStored();
    tribe.addFood(1);
    assert.deepEqual(initFood + 1, tribe.foodStored());
});

QUnit.test("cannot create a tribe without a name", function (assert) {
    assert.throws(
        function () {
            tribe = new Tribe();
        },
        function (err) {
            return err === "Tribe must have a name";
        }
    );
});