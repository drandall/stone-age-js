﻿/// <reference path="~/tests/qunit.js" />
/// <reference path="~/game/tribe_collection.js" />

QUnit.test("tribe added shows up in allTribes", function (assert) {
    var collection = new TribeCollection();
    collection.addTribe("drew");
    assert.notEqual(collection.tribes().indexOf("drew"), -1);
});

QUnit.test("tribe hidden does not show up in allTribes", function (assert) {
    var collection = new TribeCollection();
    collection.addTribe("drew");
    collection.hideTribe("drew");
    assert.equal(collection.tribes().indexOf("drew"), -1);
});

QUnit.test("next after returns next tribe", function (assert) {
    var collection = new TribeCollection();
    collection.addTribe("drew");
    collection.addTribe("cori");
    assert.deepEqual(collection.nextAfter("drew"), "cori");
});

QUnit.test("next after next tribe returns undefined if bad tribe", function (assert) {
    var collection = new TribeCollection();
    assert.deepEqual(collection.nextAfter("not a real tribe"), undefined);
});

QUnit.test("next after next tribe returns first if asked for last", function (assert) {
    var collection = new TribeCollection();
    collection.addTribe("drew");
    collection.addTribe("cori");
    assert.deepEqual(collection.nextAfter("cori"), "drew");
});

QUnit.test("shift last to first moves last to first", function (assert) {
    var collection = new TribeCollection();
    collection.addTribe("drew");
    collection.addTribe("cori");
    collection.addTribe("bob")
    collection.shiftLastToFirst();

    assert.deepEqual(collection.tribes()[0], "bob");
    assert.deepEqual(collection.tribes()[1], "drew");
    assert.deepEqual(collection.tribes()[2], "cori");
});

QUnit.test("unhideAll returns hidden tribes", function (assert) {
    var collection = new TribeCollection();
    collection.addTribe("drew");
    collection.hideTribe("drew");
    collection.unhideAll();

    assert.deepEqual(collection.tribes()[0], "drew");
});

QUnit.test("nextAfter returns undefined if all tribes are hidden", function (assert) {
    var collection = new TribeCollection();
    collection.addTribe("drew");
    collection.hideTribe("drew");

    assert.deepEqual(collection.nextAfter("drew"), undefined);
});