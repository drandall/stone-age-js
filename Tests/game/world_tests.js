﻿/// <reference path="~/tests/qunit.js" />
/// <reference path="~/game/world.js" />

QUnit.test("world has a hunting grounds", function (assert) {
    var world = new World();
    assert.notDeepEqual(world.huntingGrounds, undefined);
});

QUnit.test("world has a forest", function (assert) {
    var world = new World();
    assert.notDeepEqual(world.forest, undefined);
});