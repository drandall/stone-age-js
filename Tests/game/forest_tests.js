﻿/// <reference path="~/tests/qunit.js" />
/// <reference path="~/game/forest.js" />
/// <reference path="~/game/tribe.js" />
/// <reference path="~/game/world.js" />

var forestTestsContext;

var resetForestTestsContext = function () {

    forestTestsContext = {
        world: undefined,
        forest: undefined
    };

    forestTestsContext.world = new World();
    forestTestsContext.world.numTribes = function () { return 4; }

    forestTestsContext.forest = new Forest(forestTestsContext.world);
};

QUnit.test("forest accepts tribesmen", function (assert) {
    resetForestTestsContext();
    var forest = forestTestsContext.forest;

    var tribe = new Tribe("drew");
    forest.sendTribesmen(1, tribe);
    assert.deepEqual(forest.numTribesmenFor(tribe), 1);
});

QUnit.test("forest refuses tribesmen if it already has some for that tribe", function (assert) {
    resetForestTestsContext();
    var forest = forestTestsContext.forest;

    var tribe = new Tribe("drew");
    forest.sendTribesmen(1, tribe);
    
    assert.throws(
        function () {
            forest.sendTribesmen(1, tribe);
        },
        function (err) {
            return err === "Same tribe cannot send tribesmen twice";
        }
    );
});

QUnit.test("forest only allows seven tribesmen", function (assert) {
    resetForestTestsContext();
    var forest = forestTestsContext.forest;

    var tribe = new Tribe("drew");

    assert.throws(
        function () {
            forest.sendTribesmen(8, tribe);
        },
        function (err) {
            return err === "Forest is only big enough for seven tribesmen";
        }
    );
});

QUnit.test("forest allows tribesmen from multiple tribes", function (assert) {
    resetForestTestsContext();
    var forest = forestTestsContext.forest;

    var tribe = new Tribe("drew");
    var tribe2 = new Tribe("cori");
    forest.sendTribesmen(1, tribe);
    forest.sendTribesmen(2, tribe2);
    assert.deepEqual(forest.numTribesmenFor(tribe2), 2);
});

QUnit.test("forest only allows one tribe if there are two tribes in the world", function (assert) {
    resetForestTestsContext();
    forestTestsContext.world.numTribes = function () { return 2; }
    var forest = forestTestsContext.forest;

    var drew = new Tribe("drew");
    forest.sendTribesmen(1, drew);

    var cori = new Tribe("cori");

    assert.throws(
        function () {
            forest.sendTribesmen(2, cori);
        },
        function (err) {
            return err === "Forest can only contain one tribe when there are two tribes in the world";
        }
    );
});

QUnit.test("forest only allows two tribes if there are three tribes in the world", function (assert) {
    resetForestTestsContext();
    forestTestsContext.world.numTribes = function () { return 3; }
    var forest = forestTestsContext.forest;

    var drew = new Tribe("drew");
    forest.sendTribesmen(1, drew);

    var cori = new Tribe("cori");
    forest.sendTribesmen(2, cori);

    var fred = new Tribe("fred");

    assert.throws(
        function () {
            forest.sendTribesmen(1, fred);
        },
        function (err) {
            return err === "Forest can only contain two tribes when there are three tribes in the world";
        }
    );
});

QUnit.test("forest only allows three tribes if there are four tribes in the world", function (assert) {
    resetForestTestsContext();
    forestTestsContext.world.numTribes = function () { return 4; }
    var forest = forestTestsContext.forest;

    var drew = new Tribe("drew");
    forest.sendTribesmen(1, drew);

    var cori = new Tribe("cori");
    forest.sendTribesmen(2, cori);

    var fred = new Tribe("fred");
    forest.sendTribesmen(1, fred);

    var ted = new Tribe("ted");

    assert.throws(
        function () {
            forest.sendTribesmen(1, ted);
        },
        function (err) {
            return err === "Forest can only contain three tribes when there are four tribes in the world";
        }
    );
});