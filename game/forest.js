﻿function Forest(world) {

    function TribeCount(tribe, count) {
        this.tribe = tribe;
        this.count = count;
    }

    var tribeCounts = [];

    this.sendTribesmen = function (numTribesmen, tribe) {

        if (this.numTribesmenFor(tribe) !== 0) {
            throw "Same tribe cannot send tribesmen twice";
        }

        if (numTribesmen > 7) {
            throw "Forest is only big enough for seven tribesmen";
        }

        if (world.numTribes() === 2 && tribeCounts.length >= 1) {
            throw "Forest can only contain one tribe when there are two tribes in the world";
        } else if (world.numTribes() === 3 && tribeCounts.length >= 2) {
            throw "Forest can only contain two tribes when there are three tribes in the world";
        } else if (world.numTribes() === 4 && tribeCounts.length >= 3) {
            throw "Forest can only contain three tribes when there are four tribes in the world";
        }

        tribeCounts.push(new TribeCount(tribe, numTribesmen));
    };

    var tribeCountFor = function (tribe) {

        for (var i = 0; i < tribeCounts.length; i++) {
            var tribeCount = tribeCounts[i];
            if (tribeCount.tribe === tribe) {
                return tribeCount;
            }
        }
        return undefined;
    };

    this.numTribesmenFor = function (tribe) {

        var tribeCount = tribeCountFor(tribe);

        if (tribeCount === undefined) {
            return 0;
        }

        return tribeCount.count;
    };
}