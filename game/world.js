﻿/// <reference path="~/game/hunting_grounds.js" />
/// <reference path="~/game/forest.js" />

function World() {

    this.huntingGrounds = new HuntingGrounds();

    this.forest = new Forest();
}