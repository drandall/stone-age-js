﻿function HuntingGrounds() {
    var hunters = { };

    this.d6 = new D6();

    this.sendHunters = function (numHunters, tribe) {
        if (numHunters === 0) {
            throw "Cannot send zero hunters";
        };

        if (hunters[tribe] !== undefined) {
            throw "Same tribe cannot send hunters twice";
        }

        tribe.useTribesmen(numHunters);
        hunters[tribe] = numHunters;
    };

    this.numHunters = function (tribe) {
        if (tribe === undefined
            || hunters[tribe] === undefined) {
            return 0;
        };

        return hunters[tribe];
    };

    this.clear = function () {
        hunters = function () { };
    };

    this.hunt = function (tribe) {

        var toRoll = this.numHunters(tribe);
        var totalRoll = 0;

        for (var i = 0; i < toRoll; i++) {
            totalRoll += this.d6.roll();
        };

        tribe.returnTribesmen(this.numHunters(tribe));
        hunters[tribe] = undefined;

        var foodGathered = Math.floor(totalRoll / 2);

        tribe.addFood(foodGathered);

        return foodGathered;
    };
}