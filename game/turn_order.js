﻿/// <reference path="~/game/tribe_collection.js" />

function TurnOrder(tribeNames) {

    if (!(tribeNames instanceof Array)) {
        throw "tribeNames must be an array";
    }

    if (tribeNames.length < 2) {
        throw "tribeNames must have at least two tribes";
    }

    if (tribeNames.length > 4) {
        throw "tribeNames must have less than five tribes";
    }

    for (var i = 0; i < tribeNames.length; i++) {
        var tribeName = tribeNames[i];
        if (!(typeof tribeName == 'string' || tribeName instanceof String)) {
            throw "tribeNames must be strings";
        }
    }

    var tribesCollection = new TribeCollection();

    while (tribeNames.length > 0) {
        var index = Math.floor(Math.random() * tribeNames.length);
        tribesCollection.addTribe(new Tribe(tribeNames[index]));
        tribeNames.splice(index, 1);
    }

    var currentTribeRef = tribesCollection.tribes()[0];

    this.remainingTribes = function () {
        return tribesCollection.tribes();
    };

    this.phase = function () {
        return "sending tribesmen";
    };

    this.currentTribe = function () {
        return currentTribeRef;
    };

    this.endTurn = function () {
        if (currentTribeRef.availableTribesmen() === 0) {
            tribesCollection.hideTribe(currentTribeRef);
        }

        currentTribeRef = tribesCollection.nextAfter(currentTribeRef);
    };
}