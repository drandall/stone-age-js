﻿function TribeCollection() {

    var tribesArray = [];

    var tribesHiddenArray = [];

    this.addTribe = function(tribe) {
        tribesArray.push(tribe);
    };

    this.tribes = function() {
        var tempArray = tribesArray.slice();

        for (var i = 0; i < tribesHiddenArray.length; i++) {
            var indxOfHidden = tempArray.indexOf(tribesHiddenArray[i]);
            tempArray.splice(indxOfHidden, 1);
        }

        return tempArray;
    };

    this.hideTribe = function (tribe) {
        var index = tribesArray.indexOf(tribe);
        if (index !== -1) {
            tribesHiddenArray.push(tribe);
        }
    };

    this.nextAfter = function (tribe) {

        var index = tribesArray.indexOf(tribe);

        if (index === -1) {
            return undefined;
        }

        if (tribesHiddenArray.length === tribesArray.length) {
            return undefined;
        }

        index += 1;

        if (index === tribesArray.length) {
            index = 0;
        }

        var next = tribesArray[index];

        if (tribesHiddenArray.indexOf(next) !== -1) {
            return this.nextAfter(next);
        }
        else {
            return next;
        }
    };

    this.shiftLastToFirst = function () {
        tribesArray.unshift(tribesArray.pop());
    };

    this.unhideAll = function () {
        tribesHiddenArray = [];
    };
}