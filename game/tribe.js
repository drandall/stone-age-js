﻿function Tribe(name) {
    if (name === undefined) {
        throw "Tribe must have a name";
    };

    this.name = name;
    
    var numTribesmen = 5;
    var availableTribesmen = 5;
    var foodStoredNum = 12;

    this.sizeOfTribe = function () {
        return numTribesmen;
    };

    this.availableTribesmen = function () {
        return availableTribesmen;
    };

    this.useTribesmen = function (numTribesmen) {

        if (availableTribesmen < numTribesmen) {
            throw "Not enough available tribesmen";
        };

        availableTribesmen -= numTribesmen;
    };

    this.returnTribesmen = function (numTribesmen) {
        availableTribesmen += numTribesmen;
    };

    this.foodStored = function () {
        return foodStoredNum;
    };

    this.addFood = function (numFood) {
        foodStoredNum += numFood;
    }
}